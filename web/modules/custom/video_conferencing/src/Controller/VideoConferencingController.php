<?php
namespace Drupal\video_conferencing\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides route responses for the Example module.
 */
class VideoConferencingController extends ControllerBase {
  
  /**
   * get options
   *
   */
  public function render() {
    $config = \Drupal::config('video_conferencing.settings');
    $baseUrl = $config->get('video_conferencing_base_url');
    
    $output = [];
    
    $output['content'] = [
      '#theme' => 'video_conferencing',
      '#list' => [],
      '#attached' => [
        'library' => ['video_conferencing/video_conferencing'],
        'drupalSettings' => [
          'videoConferenceUrl' => $baseUrl,
          'publicRoomIdentifier' => 'dashboard',
          'sessionid' => 'Course 01',
          'open' => 'true',
        ],
      ],
    ];
    
    return $output;
  }
  
  /**
   * get options
   *
   */
  public function videoConference($conference_id, $host = false) {
    $entity = \Drupal::entityManager()->loadEntityByUuid('node', $conference_id);
    $current_user = \Drupal::currentUser();
    $roles = $current_user->getRoles();
    
    if(!(in_array('staff', $roles) || in_array('super_admin', $roles) || in_array('administrator', $roles)) && ($host == true)) {
      $url = Url::fromRoute('system.404');
      $response = new RedirectResponse($url->toString());
      $response->send();
      return;
    }
    
    $fullName = 'Guest';
    if(!empty($current_user)) {
      $fullName = $current_user->getUsername();
    }
    
    if(!empty($entity)) {
      $config = \Drupal::config('video_conferencing.settings');
      $baseUrl = $config->get('video_conferencing_base_url');
      
      $output = [];
      
      $output['content'] = [
        '#theme' => 'video_conferencing_host',
        '#variables' => [
          'meetingTitle' => $entity->getTitle(),
          'host' => $host,
        ],
        '#attached' => [
          'library' => [
            'video_conferencing/video_conferencing_header',
            'video_conferencing/video_conferencing_footer',
          ],
          'drupalSettings' => [
            'videoConferenceUrl' => $baseUrl,
            'publicRoomIdentifier' => $conference_id,
            'sessionid' => $conference_id,
            'open' => $host,
            'userFullName' => $fullName,
          ],
        ],
      ];
      
      return $output;
    }
    
    \Drupal::messenger()->addError('Meeting not Exist!');
    
    $response = new RedirectResponse(Url::fromRoute('<front>')->toString());
    $response->send();
    return;
  }
  
  /**
   * get options
   *
   */
  public function startConference($conference_id) {
    return $this->videoConference($conference_id, true);
  }
  
  /**
   * get options
   *
   */
  public function joinConference($conference_id) {
    return $this->videoConference($conference_id, false);
  }
  
  
}