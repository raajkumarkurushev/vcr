<?php

namespace Drupal\video_conferencing\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class VideoConferencingySettingsForm extends ConfigFormBase {
    /** @var string Config settings */
  const SETTINGS = 'video_conferencing.settings';

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'video_conferencing_settings_form';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    
    $form['video_conferencing_base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site Base Url'),
      '#default_value' => $config->get('video_conferencing_base_url'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
      // Retrieve the configuration
      $this->configFactory->getEditable(static::SETTINGS)
        ->set('video_conferencing_base_url', $form_state->getValue('video_conferencing_base_url'))
        ->save();

    parent::submitForm($form, $form_state);
  }
}
