<?php

namespace Drupal\journal\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class JournalTermVolumeForm extends FormBase {

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'journal_term_volume_form';
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    //_journal_delete_terms_by_vid('volume');

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import Term Volume'),
    ];

    return $form;
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    _journal_get_term_volume();
  }

}
