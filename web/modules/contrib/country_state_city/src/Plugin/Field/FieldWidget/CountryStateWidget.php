<?php

namespace Drupal\country_state_city\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\Query\QueryFactory;

/**
 * Plugin implementation of the 'country_state_widget' widget.
 *
 * @FieldWidget(
 *   id = "country_state_widget",
 *   label = @Translation("Country state widget"),
 *   field_types = {
 *     "country_state_type"
 *   }
 * )
 */
class CountryStateWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * File storage for files.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * File storage for files.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */

  protected $entityQuery;

  /**
   * Construct a MyFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Defines an interface for entity field definitions.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   Entity query service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entityTypeManager, QueryFactory $entity_query) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->entityTypeManager = $entityTypeManager;
    $this->entityQuery = $entity_query;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      // Add any services you want to inject here.
      $container->get('entity_type.manager'),
      $container->get('entity.query')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size' => 60,
      'placeholder' => '',
    ] + parent::defaultSettings();
  }

  /**
   * Gets the initial values for the widget.
   *
   * This is a replacement for the disabled default values functionality.
   *
   * @return array
   *   The initial values, keyed by property.
   */
  protected function getInitialValues() {
    $initial_values = [
      'country' => '',
      'state' => '',
    ];

    return $initial_values;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['size'] = [
      '#type' => 'number',
      '#title' => $this->t('Size of textfield'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
      '#min' => 1,
    ];
    $elements['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Textfield size: @size', ['@size' => $this->getSetting('size')]);
    if (!empty($this->getSetting('placeholder'))) {
      $summary[] = $this->t('Placeholder: @placeholder', ['@placeholder' => $this->getSetting('placeholder')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function getStates($country_id) {
    if ($country_id) {
      $query = $this->entityQuery->get('statelist')
        ->condition('country_id', $country_id)
        ->sort('name', 'asc');

      $ids = $query->execute();

      $states = [];
      if (count($ids) == 1) {
        $result = $this->entityTypeManager->getStorage('statelist')->load(key($ids));
        $states[$result->id()] = $result->getName();
      }
      elseif (count($ids) > 1) {
        $results = $this->entityTypeManager->getStorage('statelist')->loadMultiple($ids);
        foreach ($results as $result) {
          $states[$result->id()] = $result->getName();
        }
      }

      return $states;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = $items[$delta];
    $value = $item->getEntity()->isNew() ? $this->getInitialValues() : $item->toArray();

    $field_name = $this->fieldDefinition->getName();

    if (isset($form_state->getUserInput()[$field_name])) {
      $country_id = $form_state->getUserInput()[$field_name][$delta]['country'];
      $state_id = $form_state->getUserInput()[$field_name][$delta]['state'];
    }

    $country_id = $country_id ?? $value['country'] ?? NULL;
    $state_id = $state_id ?? $value['state'] ?? NULL;

    $query = $this->entityQuery->get('countrylist')
      ->sort('name', 'asc');

    $ids = $query->execute();

    $countries = [];
    if (count($ids) == 1) {
      $result = $this->entityTypeManager->getStorage('countrylist')->load(key($ids));
      $countries[$result->id()] = $result->getName();
    }
    elseif (count($ids) > 1) {
      $results = $this->entityTypeManager->getStorage('countrylist')->loadMultiple($ids);
      foreach ($results as $result) {
        $countries[$result->id()] = $result->getName();
      }
    }
    $div_id = 'state-wrapper-' . $field_name . '-' . $delta;

    $element['state_wrapper1']['#prefix'] = '<div id="' . $div_id . '"><div>' . $this->fieldDefinition->label() . '</div>';
    $element['state_wrapper1']['#markup'] = '';

    $element['country'] = [
      '#type' => 'select',
      '#options' => $countries,
      '#default_value' => $country_id,
      '#empty_option' => $this->t('-- Select an option --'),
      '#required' => $this->fieldDefinition->isRequired(),
      '#title' => $this->t('Country'),
      '#delta' => $delta,
      '#validated' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'ajaxFillState'],
        'event' => 'change',
        'wrapper' => $div_id,
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Searching states...'),
        ],
      ],
    ];

    if ($country_id) {
      $element['state'] = [
        '#type' => 'select',
        '#default_value' => $state_id,
        '#options' => $this->getStates($country_id),
        '#empty_option' => $this->t('-- Select an option --'),
        '#required' => $this->fieldDefinition->isRequired(),
        '#title' => $this->t('State'),
        '#active' => FALSE,
        '#delta' => $delta,
        '#validated' => TRUE,
      ];
    }

    $element['state_wrapper2']['#suffix'] = '</div>';
    $element['state_wrapper2']['#markup'] = '';
    return $element;
  }

  /**
   * Call the function that consume the webservice.
   *
   * @param array $form
   *   A form that be modified.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The values of the form.
   *
   * @return array
   *   The form modified
   */
  public function ajaxFillState(array $form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    $delta = $element['#delta'];

    $field_name = $this->fieldDefinition->getName();
    $form = $form[$field_name];

    unset($form['widget'][$delta]['_weight']);

    return $form['widget'][$delta];
  }

}
